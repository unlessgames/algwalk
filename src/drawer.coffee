drawer =
  cx : null
  offset : new v2(0,0)
  midiio : () ->
    @cx.font = (Texts.fontsize * 0.5) + "px " + Texts.family;
    @cx.fillStyle = "#fff"
    @cx.textAlign = "left"
    y = Texts.fontsize * 0.5
    cxt = @cx
    write = (t) ->
      cxt.fillText(t,0,y)
      y += (Texts.fontsize * 0.5) + 3
    write "IN:"
    for i in WebMidi.inputs
      write "#{i.name}"
    write ""
    write "OUT:"
    for o in WebMidi.outputs
      write "#{o.name}"
  offsetpos : (p) -> new v2(p.x + @offset.x, p.y + @offset.y) 
  clampname : (n) -> if n.length > 23 then n.slice(0,20) + "..." else n.slice(0, 23)
  line : (a, b, c) ->
    @cx.strokeStyle = c
    @cx.beginPath()
    a = @offsetpos a
    b = @offsetpos b
    @cx.moveTo(a.x, a.y)
    @cx.lineTo(b.x, b.y)
    @cx.stroke()
  centertext : (p,text, c, s, align = "center", baseline = "middle") ->
    @cx.font = s + "px " + Texts.family;
    @cx.lineWidth = 2
    @cx.fillStyle = c
    @cx.textAlign = align
    @cx.textBaseline = baseline
    @cx.fillText(text,p.x, p.y);
  midi : (m, input, sel = false, available = true) ->
    c = if not available then "#a41" else "#666"
    @cx.strokeStyle = c
    p = @offsetpos m.pos
    w = 256
    pin = w / 16
    @cx.strokeRect(p.x, p.y, w, 30)
    for channel, i in m.outputs
      pinpos = add2d(p, new v2(pin*i, (if input then 30 else -pin)))
      pincolor = 40 +
        if (i + 1) % 4 is 0 
          0 
        else 
          25 
      # pincolor = if i < 4 or i >= 8 and i < 12 then 40 else 20
      # pincolor += (i % 2) * 40
      @cx.fillStyle = 
        if not available          
          c
        else if channel isnt null
          Colors.connected
        else 
          "rgb(#{pincolor}, #{pincolor}, #{pincolor})" 
      @cx.fillRect(pinpos.x,pinpos.y, pin, pin)
    text = 
      if sel
        m.self.name 
      else 
        (if input then "INPUT" else "OUTPUT") + (if available then " [#{@clampname(m.self.name)}]" else " PORT DISCONNECTED!")
    @centertext(add2d(p, new v2(w * 0.5, 16)), text, c, Texts.fontsize * 0.5)
  arrow : (a, b, next, t = null) ->
    np = b
    d = distance(a, np)
    t = 60 / d if !t?
    s = lerp2d(a, np, t)
    # if not started
    @cx.lineWidth = 2
    @line(np, a, "#666")
    @cx.lineWidth = 4
    @cx.strokeStyle = "#fff"
    @line(a, s,(if next is 0 then Colors.neutral else if next is 1 then Colors.connected else Colors.random), @cx)
  pin : (pos, channel, input = true) ->
    p = @offsetpos pos
    @cx.fillStyle = Colors.connected
    @cx.fillRect(p.x, p.y, 16,16)
    @cx.fillStyle = "#000"
    @cx.fillRect(p.x, p.y - 16, 16,16)
    @centertext(add2d(p, new v2(8, 0)), channel, Colors.neutral, 14, "middle", "bottom")    
  nodecolor : (t) ->
    if t.type is "stepper" and t.self.step > 0 then Colors.ascending else Colors.descending
  getintervalname : (s, acc = 0) ->
    if s < 0 
      s = Math.abs s
    if s > 12
      @getintervalname(s - 12, acc + 1)
    else
      (if acc is 0 then "" else acc)  + Names.intervals[s]
  nodetext : (n, c) ->
    text = 
      switch n.type
        when "stepper" 
          # n.self.offset + (if n.self.step is 0 then "" else (if n.self.step > 0 then "+" + n.self.step else n.self.step))
          n.self.offset + (if n.self.step is 0 then "" else (if n.self.step > 0 then "+" else "-") + 
                           if Settings.intervalnotation then @getintervalname(Math.abs(n.self.step)) else Math.abs(n.self.step))
        when "grapher"
          n.self.graphname
    pos = @offsetpos(new v2(n.pos.x, n.pos.y), OFFSET)
    @centertext(pos,text, c, Texts.fontsize)
  poly : (p, r, rs, c, n, o = 0) ->
    @cx.strokeStyle = c
    @cx.beginPath()
    @cx.translate(p.x, p.y)
    fst = null
    for i in [0..n]
      t = i / n
      x = (r + random() * rs * r) * Math.sin(Math.PI * 2 * t + o)
      y = (r + random() * rs * r) * Math.cos(Math.PI * 2 * t + o)
      if i is 0
        fst = new v2(x, y)
        @cx.moveTo(x, y)
      else if i is n
        @cx.lineTo(fst.x, fst.y)
      else
        @cx.lineTo(x, y)
    @cx.translate(-p.x, -p.y)
  nodebody : (d) ->
    @cx.fillStyle = if d.editorstate is "edited" and d.playing then "#406050" else if d.editorstate is "edited" then "#123" else if d.playing then "#543" else "black"
    @cx.strokeStyle = Colors[d.editorstate]
    @cx.lineWidth = 3 + Math.random() * 1
    @cx.beginPath()
    p = @offsetpos d.node.model.pos
    radius = if d.editorstate is "edited" then Sizes.noderadius + 2 else Sizes.noderadius
    if d.editorstate is "edited" 
      @poly(p, radius, 0.03, @cx.strokeStyle, Math.floor(16 + Math.random() * 2))
    else
      @cx.arc(p.x, p.y,radius,0,2*Math.PI)
    @cx.stroke()
    @cx.globalAlpha = 0.6
    @cx.fill()
    @cx.globalAlpha = 0.32
  startnode : (pos) ->
    @poly(@offsetpos(pos), Sizes.noderadius * 1.24, 0.01, Colors.connected, 4, Math.PI * 0.25)
    @cx.stroke()
  oncircle : (r, rad, _offset = null) ->
    new v2(Math.sin(rad) * r, Math.cos(rad) * r)
  commands : (d) ->
    n = d.node
    p = @offsetpos n.model.pos
    # @cx.beginPath()
    # @cx.fillStyle = "#000"
    # @cx.arc(p.x + 20, p.y + 20,Sizes.noderadius * 1,0,2*Math.PI)
    # @cx.fill()
    @centertext(add2d(p, @oncircle(Sizes.noderadius + 16, Math.PI * 0.25)), "X", Colors.warning, 16, "center")
    if n.model.type is "stepper"
      #new v2(-Sizes.noderadius - 8, -8)
      c = (if n.model.self.offset is "@" then Colors.connected else Colors.neutral)
      @centertext(add2d(p, @oncircle(Sizes.noderadius + 16, Math.PI * 1.25)), "Q@", c, 14, "center")
      c = (if n.model.exiting is "random" then Colors.random else Colors.neutral)
      @centertext(add2d(p, @oncircle(Sizes.noderadius + 16, Math.PI * 0.75)), "R", c, 14, "center")
      c = (if d.startnode then Colors.connected else Colors.neutral)
      ap = add2d(p, new v2(-Sizes.noderadius - 16, 0))
      @centertext(ap, "A", c, 14)
      @cx.lineWidth = 1
      @cx.strokeRect(ap.x-10, ap.y-10, 20, 20)
      @cx.lineWidth = 1
      @centertext(add2d(p, new v2(0, -Sizes.noderadius - 22)), "+", Colors.connected, 14)
      @centertext(add2d(p, new v2(0, -Sizes.noderadius - 10)), "W", Colors.connected, 14)
      @centertext(add2d(p, new v2(0, Sizes.noderadius+ 10)), "S", Colors.connected, 14)
      @centertext(add2d(p, new v2(0, Sizes.noderadius+ 18)), "-", Colors.connected, 14)
  node : (drawable, igraph) ->
    if drawable.startnode then @startnode drawable.node.model.pos
    @renderoutputs(drawable.node, igraph) if igraph?
    @nodebody(drawable)
    @nodetext(drawable.node.model, Colors[drawable.editorstate])
    if drawable.editorstate is "edited" then @commands drawable
  init : (o, ctx) ->
    @offset = o
    @cx = ctx
    console.log(@cx)

  nodestatetocolor : (id, selected, connected, edited) ->
    c = if !connected? or connected.object.type is "pinin" then -1 else connected.id
    s = if !selected? or selected.object.type in ["pinout", "pinin", "in", "out"] then -1 else selected.id
    e = if !edited? then -1 else edited.id
    if id is e then "edited" 
    else if id is c then "connected" 
    else if id is s and c isnt -1 and c isnt s then "selected"
    else if id is s then "selected" 
    else "neutral"

  renderoutputs : (n, nodes) ->
    outputs = n.getnextoutputs()
    if outputs isnt null
      for o in outputs
        # console.log o.id
        # __ igraph
        if n.model.exiting is "random"
          @arrow(n.model.pos, nodes[o.id].model.pos, 2)
        else
          @arrow(n.model.pos, nodes[o.id].model.pos, o.next)


  renderigraph : (igraph, selected, connected, edited) ->
    nodes = igraph.inodes
    for n in [0..nodes.length - 1]
      t = @nodestatetocolor(n, selected, connected, edited)
      d = new drawablenode(nodes[n], t, igraph.currents[0] is n, igraph.startnode is n)
      @node(d, igraph.inodes)



  pinoffset : (i, input = false) -> 
    new v2((16)*i + 8, (if input then 38 else -10))
  renderrooting : (rooting, selected, connected, edited) ->
    for i, j in rooting.inputs
      @midi(i.model, true, (selected isnt null and selected.id is j and selected.object.type is "in"), WebMidi.getInputByName i.model.self.name)
      for c, index in i.model.outputs
        if c isnt null
          if c.type is "grapher"
            connectedpos = rooting.igraph.inodes[c.id].model.pos
            @arrow(add2d(i.model.pos, @pinoffset(index, true)), connectedpos, 1)
          else if c.type is "out"
            connectedpos = add2d(rooting.outputs[c.id].model.pos, @pinoffset(c.outpin))
            @arrow(add2d(i.model.pos, @pinoffset(index, true)), connectedpos, 1, 1)
    for o, k in rooting.outputs
      @midi(o.model, false,  (selected isnt null and selected.id is k and selected.object.type is "out"), WebMidi.getInputByName o.model.self.name)
    for n, i in rooting.igraph.inodes
      t = @nodestatetocolor(i, selected, connected, edited)
      d = new drawablenode(n, t)
      outputs = n.getnextoutputs()
      if outputs?
        for c in outputs
          bpos = 
            if c.model.type is "out"
              add2d(rooting.outputs[c.id].model.pos, @pinoffset(c.model.pin, false))
            else
              rooting.igraph.inodes[c.id].model.pos
          @arrow(n.model.pos, bpos, c.next)
      @node(d)

  render : (current,rooting, igraph, selected, connected, edited, mouse) ->
    if selected?
      if selected.object.type in ["pinin", "pinout"]
        @pin(selected.object.pos, selected.object.pin+1, selected.object.type is "pinin")

    if connected?
      a = 
        if connected.object.type is "pinin" 
          add2d(connected.object.pos, new v2(8,8))
        else
          connected.object.model.pos
      b = 
        if selected?
          if selected.object.type is "pinin" or selected.object.type is "pinout" 
            add2d(selected.object.pos, new v2(8,8))
          else if selected.object.type isnt "out" and selected.object.type isnt "in"
            selected.object.model.pos 
          else mouse
        else 
          mouse
      linec = 
        if connected.object.type isnt "pinin"
          if selected? and connected?
            if connected.object.model.outputs.reduce(((a, v) -> if v.id is selected.id then true else a), false) 
              Colors.warning 
            else Colors.connected
          else
            Colors.connected
        else 
          Colors.connected
      @line(a, b, (if selected? then linec else Colors.warning))

    if current is "rooting"
      @renderrooting(rooting, selected, connected, edited)
    else
      @renderigraph( igraph, selected, connected, edited)
class drawablenode
  constructor : (@node, @editorstate, @playing = false, @startnode = false) ->
