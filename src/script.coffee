
CTX = null
CONNECTED = null
EDITED = null
DRAGGED = null
OFFSET = new v2(0,0) 
MOUSE = new v2(0,0)
PROJECT = null
CURRENT = "rooting"
IGRAPH = null

Editor =
  change : (n, operation, arg) ->
    switch n.type
      when "stepper"
        switch operation
          when "startnode"
            console.log PROJECT.rooting.igraph.inodes.find((g) -> g.model.self.graphname is CURRENT).igraph
            # console.log n
            PROJECT.rooting.igraph.inodes.find((g) -> g.model.self.graphname is CURRENT).igraph.startnode = arg
          when "write"
            if arg is "q"
              n.self.offset = if n.self.offset is "@" then "" else "@"
            else
              n.self.step = 
                parseInt((if n.self.step is 0 then arg else n.self.step + arg), 10)
          when "del"
            s = n.self.step + ""
            n.self.step = if s.length > 1 then parseInt(s.slice(0,-1), 10) else 0
          when "change"
            n.self.step = clamp(-127, 127, n.self.step + arg)
          when "randomexit"
            n.exiting = if n.exiting is "random" then "iterator" else "random"
  remove : (n) ->
    __ n
    if n.object.model.type is "grapher"
      if CURRENT is "rooting"
        if confirm("delete graph '#{n.object.model.self.graphname}' with #{n.object.igraph.inodes.length} nodes in it?")
          PROJECT.rooting.remove({type:"grapher", id:n.id})
          delete PROJECT.model.graphs[n.object.model.self.graphname]
          graphbuttons("rooting")

    else
      if CURRENT isnt "rooting"
        __ "removing node"
        id = PROJECT.rooting.igraph.inodes.find((n) -> n.model.self.graphname is CURRENT).igraph.remove({type:"stepper", id:n.id})
        EDITED = {id:id, object:IGRAPH.inodes[id]}

    #connect channels
  # connect : (id) ->
    # if @type is "input"
      # @self.addListener("noteon", )


# map = 
#   root : new map

console.log "ls"

getnewgraphname = () ->
  letters = Names.graphs.split("")
  gc = letters.filter((l) -> Object.keys(PROJECT.model.graphs).findIndex((a) -> a is l) is -1)
  if gc.length is 0
    null
  else
    gc[Math.floor(Math.random() * gc.length)]


resize = () ->
  CTX.canvas.width = window.innerWidth
  CTX.canvas.height = window.innerHeight
  OFFSET = new v2(CTX.canvas.width * 0.5, CTX.canvas.height  * 0.5)
  drawer.offset = OFFSET

notehistory = []

traverserooting = (r, acc = 0) ->
  if acc is 100 then null
  else
    console.log r
    r = PROJECT.rooting.igraph.inodes[r.next.id].applynote(PROJECT.rooting.igraph.currentnote, r.note)
    switch r.next.type
      when "out" then r
      when "untied" then null
      else traverserooting(r, acc + 1)

noteon = (project, e) ->
  __ e
  for inputnode in project.rooting.inputs
    if inputnode.model.self.name is e.target.name
      if inputnode.model.outputs[(e.channel) - 1]?
        cable = inputnode.model.outputs[(e.channel) - 1]
        if cable.type is "out"
          project.rooting.outputs[cable.id].midi.playNote(e.note.number, cable.outpin + 1, {velocity:e.velocity})
        else   
          id = cable.id
          # r = project.rooting.igraph.inodes[id].applynote(e.note.number, e.note.number)
          n = {note:e.note.number, next:{id:id}}
          r = traverserooting(n)
        
          # while r.next.type isnt "out" or r.next.type isnt "untied"
          # r = project.rooting.igraph.inodes[r.next.id].applynote(r.note, r.note)
          if r?
            # console.log "playing note"
            # __ "#{e.note.number} -> #{r.note}"
            # pastnote = notehistory.findIndex((n) -> n.note is r.note and n.channel is r.next.channel + 1)
            # if pastnote isnt -1
            #   pastnote = notehistory.splice(pastnote, 1)
            #   project.rooting.outputs[pastnote.id].midi.stopNote(pastnote.note, pastnote.channel)
            if notehistory.length > 0 
              pastnote = notehistory[0]
              project.rooting.outputs[pastnote.id].midi.stopNote(pastnote.note, pastnote.channel)
              notehistory.shift()
            __ r.next.channel
            notehistory.push({id : r.next.id, note : r.note, channel: r.next.pin + 1})
            project.rooting.outputs[r.next.id].midi.playNote(r.note, r.next.pin + 1, {velocity:e.velocity})

noteoff = (project, e) ->
  for inputnode in project.rooting.inputs
    if inputnode.model.self.name is e.target.name
      if inputnode.model.outputs[(e.channel) - 1]?
        cable = inputnode.model.outputs[(e.channel) - 1]
        if cable.type is "out"
          project.rooting.outputs[cable.id].midi.stopNote(e.note.number, cable.outpin+1)

switchedited = (g) ->
  CURRENT = g
  graphbuttons(g)
  IGRAPH = 
    if CURRENT is "rooting"
      $("#rootingbutton").addClass("currentgraph") 
      $("#midiportsbutton").show() 
      PROJECT.rooting.igraph
    else 
      $("#rootingbutton").removeClass("currentgraph") 
      $("#midiportsbutton").hide() 
      $("#midiports").html ""
      id = PROJECT.rooting.igraph.inodes.findIndex((e) -> e.model.self.graphname is CURRENT)
      # __ id
      if id is -1 then PROJECT.rooting.igraph else PROJECT.rooting.igraph.inodes[id].igraph
init = () ->
  WebMidi.enable((err) ->
    project = 
      if window.localStorage.getItem("map")?
        JSON.parse(window.localStorage.getItem("map"))
      else
        i = (if WebMidi.inputs.length > 0 then WebMidi.inputs[0].name else "no input")
        o = (if WebMidi.outputs.length > 0 then WebMidi.outputs[0].name else "no input")
        newproject(i, o)
    __ project
    PROJECT = new iproject project
    console.log PROJECT
    CURRENT = "rooting"
    # CURRENT = Object.keys(PROJECT.model.graphs)[0]
    switchedited(CURRENT)

    addlisteners = () ->
      for i in WebMidi.inputs
        i.addListener("noteon", "all", (e) -> noteon(PROJECT, e))
        i.addListener("noteoff", "all", (e) -> noteoff(PROJECT, e))
    addlisteners()
    console.log(WebMidi.inputs)
    console.log(WebMidi.outputs)
    # WebMidi.getInputByName("out").addListener("noteon", "all", ((n) -> console.log n))
    CTX = document.getElementById("canvas").getContext("2d")
    CTX.imageSmoothingEnabled = false
    resize()
    drawer.init(OFFSET, CTX)
    bindevents()


    # EDITED = select(PROJECT.rooting.igraph.inodes[0].igraph.inodes[1].model.pos)

    window.requestAnimationFrame(render)
  )

getclosest = (ns, p) ->
  d = Sizes.noderadius
  f = -1
  for i in [0..ns.length - 1]
    nd = Math.min(distance(p, ns[i].model.pos), d)
    if nd isnt d then f = i
    d = nd
  f


insiderect = (p, bp, s) ->
  p.x > bp.x and p.x < bp.x + s.x and p.y > bp.y and p.y < bp.y + s.y

checkioboxes = (p) ->
  obj = null
  ii = -1
  for i, index in PROJECT.rooting.inputs
    if insiderect(p, i.model.pos, new v2(256, 32))
      obj = i.model
      ii = index
    for c, kindex in i.model.outputs
      pinp = add2d(i.model.pos, new v2(kindex * 16, 30))
      if insiderect(p, pinp, new v2(16, 16))
        obj = {type:"pinin", pos:pinp, pin:kindex}
        ii = index
  for o, jndex in PROJECT.rooting.outputs
    if insiderect(p, o.model.pos, new v2(256, 32))
      obj = o.model
      ii = jndex
    for c, kindex in o.model.outputs
      pinp = add2d(o.model.pos, new v2(kindex * 16, -16))
      if insiderect(p, pinp, new v2(16, 16))
        obj = {type:"pinout", pos:pinp, pin:kindex}
        ii = jndex
  if ii is -1 then null else {id:ii, object:obj}
  
select = (p) ->

  # {id : int, object : inode | cable }
  if CURRENT is "rooting"
    selio = checkioboxes(p)
    if selio is null  
      r = getclosest( IGRAPH.inodes, p)
      if r is -1 then null else {id:r, object:IGRAPH.inodes[r]}
    else 
      selio
  else
    r = getclosest( IGRAPH.inodes, p)
    if r is -1 
      null 
    else 
      {id:r, object:IGRAPH.inodes[r]}

render = () ->
  if PROJECT isnt null
    CTX.globalAlpha = 0.32
    CTX.fillStyle = "#000"
    CTX.fillRect(0,0,CTX.canvas.width, CTX.canvas.height)
    # CTX.clearRect(0,0,CTX.canvas.width, CTX.canvas.height)
    selected = select MOUSE
    drawer.render(CURRENT,PROJECT.rooting, IGRAPH, selected, CONNECTED, EDITED, MOUSE)

  window.requestAnimationFrame(render)



mouseup = (e) ->
  selected = select(MOUSE)
  if DRAGGED? 
    if not (DRAGGED.object.type in ["in", "out"])
      EDITED = DRAGGED
    DRAGGED = null

  if DRAGGED is null and e.button is 2 and selected isnt null and CONNECTED isnt null
    if CONNECTED.object.type is "pinin" and selected.object.type isnt "pinin"
      console.log "connecting from input"
      PROJECT.rooting.inputs[CONNECTED.id].connect( selected, CONNECTED.object.pin )
    else if selected.object.type is "pinout" and CONNECTED.object.type isnt "pinout" and CONNECTED.object.type isnt "pinin"
      PROJECT.rooting.outputs[selected.id].connect( CONNECTED, selected.object.pin )
      CONNECTED.object.connect selected
      #or selected.type is "pinin"selected.id isnt CONNECTED.id or selected.type isnt CONNECTED.object.model.type
    else if not (selected.object.type in ["pinout", "pinin","in","out"]) and selected.id isnt CONNECTED.id 
      CONNECTED.object.connect selected
      EDITED = CONNECTED
    else if not (CONNECTED.object.type in ["pinin", "pinout", "in", "out"])
      EDITED = CONNECTED
  if CONNECTED? and !selected?
    addnode(new v2(e.offsetX - OFFSET.x, e.offsetY - OFFSET.y))
    if CONNECTED.object.type is "pinin"
      __ "insta connect pinin"
      PROJECT.rooting.inputs[CONNECTED.id].model.outputs[CONNECTED.object.pin] = new cable(EDITED.id, "grapher", CONNECTED.object.pin)
    else
      CONNECTED.object.connect EDITED    
  CONNECTED = null


newnode = (p) ->
  if CURRENT is "rooting" 
    name = getnewgraphname()
    if name isnt null
      PROJECT.model.graphs[name] = new graph([new node("stepper", new stepper("@",0), "iterator", new v2(0,0), [])])
      n = new inode(new node("grapher",new grapher(name),"iterator",p,[]), PROJECT.rooting.igraph)
      n.igraph = new igraph(PROJECT.model.graphs, name)
      graphbuttons("")
      {inode : n, name:name}
    else null
  else  
    n = new inode(new node("stepper",new stepper("@", 0),"iterator",p,[]),PROJECT.rooting.igraph.inodes[PROJECT.rooting.igraph.inodes.findIndex((e) -> e.model.self.graphname is CURRENT)].igraph)
    {inode : n, name:CURRENT}


addnode = (p) ->
  n = newnode(p)
  if n.inode?
    IGRAPH.inodes.push(n.inode)
    index = PROJECT.rooting.igraph.inodes.findIndex((e) -> e.model.self.graphname is n.name)
    console.log index
    IGRAPH.inodes.map((_n) -> _n.selfgraph = PROJECT.rooting.igraph.inodes[index].igraph)
    EDITED = {id:IGRAPH.inodes.length - 1, object: IGRAPH.inodes[IGRAPH.inodes.length - 1]}
    
bindevents = () ->
  $("#canvas").bind("contextmenu", (e) ->
      e.preventDefault()
      selected = select(MOUSE)
      if !selected?
        addnode(new v2(e.offsetX - OFFSET.x, e.offsetY - OFFSET.y))
  )
  $("#canvas").mousemove((e) ->
    MOUSE = new v2(e.offsetX - OFFSET.x, e.offsetY - OFFSET.y)
    limit = 10
    if e.offsetX < limit or e.offsetX > CTX.canvas.width - limit or e.offsetY < limit or e.offsetY > CTX.canvas.height - limit then mouseup({button:0})
    if DRAGGED isnt null
      io = 
        switch DRAGGED.object.type
          when "in", "out" then DRAGGED.object.type + "puts"
          else null
      if io isnt null
        PROJECT.rooting[io][DRAGGED.id].move(new v2(e.offsetX - OFFSET.x, e.offsetY - OFFSET.y))
      else if DRAGGED.object.type isnt "pinout" and DRAGGED.object.type isnt "pinin"
        IGRAPH.inodes[DRAGGED.id].move(new v2(e.offsetX - OFFSET.x, e.offsetY - OFFSET.y))

  )

  $("#canvas").mousedown((e) ->
    selected = select(MOUSE)
    if selected isnt null and e.button is 2
      if EDITED isnt null then EDITED = null
      if not (selected.object.type in ["pinout", "in", "out"]) then CONNECTED = selected;
      console.log CONNECTED
    else if selected isnt null 
      DRAGGED = selected
      console.log DRAGGED
      EDITED = null
  )
  $("#canvas").mouseup mouseup

  $("html").keydown((e) ->
    if e.ctrlKey
      e.preventDefault()
      if e.key is "d"
        d = new Date()
        # __ JSON.stringify(PROJECT.export(), 0, 4)
        $("<a />", {
            "download": "aalga-#{Object.keys(PROJECT.model.graphs).join("")}-#{d.toString().slice(0,10)}.json",
            "href" : "data:application/json," + encodeURIComponent(JSON.stringify(PROJECT.export()))
        }).appendTo("body") .click(() -> $(this).remove())[0].click()
      else if e.key is "s"
        window.localStorage.setItem("map", JSON.stringify(PROJECT.export()))
    else 
      selected = getclosest(IGRAPH.inodes, MOUSE)
      # console.log "E=#{EDITED} " + e.key
      if EDITED isnt null
        edited = IGRAPH.inodes[EDITED.id].model
        # console.log edited 
        switch e.key
          when "Enter" 
            EDITED = -1 
          when "0", "1", "2", "3", "4", "5", "6", "7", "8", "9"
            Editor.change(edited, "write", e.key)
          when "w", "ArrowUp", "+"
            Editor.change(edited, "change", 1)
          when "s", "ArrowDown", "-"
            Editor.change(edited, "change", -1)
          when "Backspace"
            Editor.change(edited, "del")
          when "q", "Q", "@"
            Editor.change(edited, "write", "q")
          when "x", "X", "Delete"
            Editor.remove(EDITED)
          when "r", "R"
            Editor.change(edited, "randomexit")
          when "a", "A"
            Editor.change(edited, "startnode", EDITED.id)
      else
        switch e.key
          when "Enter"
            if selected isnt -1 then EDITED = selected
  )

graphbuttons = (current) ->
  $("#graphlist").html("")
  for g in Object.keys(PROJECT.model.graphs)
    bt = $("<button>", {html:g, id:g+"button", class:"graphbutton #{if g is current then 'currentgraph' else ''}"})
    $("#graphlist").append bt
    $(".graphbutton").click((e) -> switchedited($(@).html()))

portbuttons = () ->
  addbuttons = (prop) ->
    for i in WebMidi[prop]
      c = 
        if PROJECT.rooting[prop].findIndex((e) -> (e.model.self.name) is (i.name)) isnt -1
          "portactive"
        else ""
      bt = $("<button>", {id:i.name, class:"portbutton #{prop} #{c}", html:i.name})
      $("#midiports").append(bt)


  $("#midiports").append($("<div>", {html:"INPUTS : "}))
  addbuttons("inputs")
  $("#midiports").append($("<div>", {html:"<br>OUTPUTS : "}))
  addbuttons("outputs")

$("#rootingbutton").click(() -> switchedited("rooting")) 
$("#midiportsbutton").click(() -> 
                            if $("#midiports").html() is "" 
                              $("#midiports").show()
                              portbuttons()
                              $(".portbutton").click((e) -> 
                                # console.log ev
                                if not $(@).hasClass("portactive") then $(@).addClass("portactive") 
                                else $(@).removeClass("portactive")
                                prop = if $(@).hasClass("outputs") then "outputs" else "inputs"
                                __ prop
                                PROJECT.rooting.toggle(prop, e.currentTarget.id)
                              )
                            else 
                              # $("#midiports").hide()
                              $("#midiports").html("")
)

$(window).load( init )
$(window).resize( resize )
