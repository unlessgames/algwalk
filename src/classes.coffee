class v2
  constructor : (@x, @y) ->
  normalize : () ->
    targetlength = 1;
    vmag = Math.sqrt(@x*@x + @y*@y);
    @x = targetlength * @x / vmag;
    @y = targetlength * @y / vmag;
    @
  scale : (s) ->
    @x *= s
    @y *= s
    @
distance = (a, b) -> Math.sqrt(Math.pow(a.x - b.x, 2)+Math.pow(a.y - b.y, 2))
clamp = (a, b, p) ->
  if a is null
    if p > b then b else p
  else if b is null 
    if p < a then a else p
  else 
    if p < a then a else if p > b then b else p
lerp = (a, b, t) -> a*(1-t)+b*t
ilerp = (a, b, p) -> (p - a) / (b - a)
lerp2d = (a, b, t) -> {x:lerp(a.x, b.x, t), y:lerp(a.y, b.y, t)}
add2d = (a, b) -> new v2(a.x + b.x, a.y + b.y)
random = () -> 1 - Math.random() * 2
__ = (t) ->
  console.log t
  t 
midiwrap = (v) -> clamp(0,127,v)

# STATIC
class cable
  constructor : (@id, @type = "stepper", @pin = 0, @outpin = 0, @points = [], @repeat = 1) ->
class midinode
  #             string  (in|out)
  constructor : (@name, @type) ->
class grapher
  #                string
  constructor : (@graphname) -> 

class stepper
  #              string    int
  constructor : (@offset, @step) ->

class exiting
  constructor : (@name) ->

class node
  #           string  (grapher|stepper|midinode) string    v2     cable[]
  constructor : (@type,       @self,             @exiting,  @pos,  @outputs) ->

class graph
  #               node[]    int 
  constructor : (@nodes, @startnode = 0) ->


class rooting
  #            node:midinode[] graph 
  constructor : (@inputs,@outputs,@graph) ->

class project
  #              rooting, obj with graph properties
  constructor : (@rooting, @graphs) ->

newproject = (i, o) ->
  mi = new midinode(i, "in")
  mo = new midinode(o, "out")
  inp = new node("in", mi, "", new v2(-128, -300), new Array(16).fill(null))
  outp = new node("out", mo, "", new v2(-128,300), new Array(16).fill(null))
  inp.outputs[0] = new cable(0, "grapher")
  outp.outputs[0] = new cable(0, "grapher")
  graphs = {}
  name = Names.graphs[Math.floor(Math.random() * Names.graphs.length)]
  graphs[name] = new graph(
          [ 
            new node("stepper", new stepper("@", 0), "iterator", new v2(0,0), [new cable(1)])
            ,new node("stepper", new stepper("", Math.floor(random() * 25)), "iterator", new v2(-100,100), [new cable(2)])
            ,new node("stepper", new stepper("", Math.floor(random() * 25)), "iterator", new v2(100,100), [new cable(0)])
          ],0)
  rootin = new rooting([inp], [outp], new graph([new node("grapher", new grapher(name), "iterator", new v2(0,0), [new cable(0,"out")])], 0)) 
  new project(rootin, graphs)

###############################################################################
# RUNTIME 


class inode 
  #               node    int     
  constructor : (@model, @selfgraph = null, @counter = 0, @igraph = null) ->
  getnextoutputs : () ->
    if @model.outputs.length > 0
      r = []
      for b in [0..@model.outputs.length - 1]
        r.push {id:@model.outputs[b].id, next : (if @counter is b then 1 else 0), model:@model.outputs[b]}
      r
    else 
      null
  countup : () ->
    # __ @model.outputs
    if @model.outputs.length > 1
      length = @model.outputs.reduce(((a, v) -> a + v.repeat), 0)
      @counter = (@counter + 1) % (length)
    else
      @counter = 0
    @counter
  connect : (selected) ->
    nt = -1
    # __ "connecting"
    # __ @model
    # __ "to"
    # __ selected

    if selected.object.type is "pinout"
      nt = @model.outputs.findIndex(((e) -> e.type is "out" and e.id is selected.id and e.pin is selected.object.pin))
    else
      nt = @model.outputs.findIndex(((e) -> e.id is selected.id and e.type is selected.object.model.type))
    # __ nt
    if nt isnt -1
      @model.outputs.splice(nt, 1)
    else
      # __ "connecting to #{selected.id}"
      @model.outputs.push new cable(selected.id,(if selected.object.type is "pinout" then "out" else selected.object.model.type), (if selected.object.type is "pinout" then selected.object.pin else 0))
  move : (p) ->
    @model.pos = p

  normalizeddistances : () ->
    distances = []
    for next, i in @model.outputs
      distances.push {d:distance(@model.pos, @selfgraph.inodes[next.id].model.pos), cable:next}
    all = distances.reduce(((a, v) -> a + v.d), 0)
    for d in distances
      d.chance = (d.d / all)
    distances
  getchances : () ->
    distances = @normalizeddistances()
    chancetable = []
    for d, i in distances
      chancetable.push { cable : d.cable, chance: d.chance }
    t = 0
    for ct in chancetable
      t += ct.chance
      ct.chance = t
    chancetable.sort((a, b) -> a.chance - b.chance)
    first = chancetable[0].chance
    for cc in chancetable
      cc.chance = 1.0 - (cc.chance - first)
    chancetable.sort((a, b) -> a.chance - b.chance)
    chancetable

  getrandom : (cht) ->
    f = false
    r = undefined
    ran = Math.random()
    if cht.length is 1 then r = cht[0].cable
    else
      i = 0
      while r is undefined
        if cht[i].chance > ran or i is cht.length - 1 then r = cht[i].cable
        else i++
    r

  exit : () ->
    if @model.outputs.length is 0
      new cable(0,"untied")
    else
      switch @model.exiting
        when "iterator"
          @countup()
          @model.outputs[@counter] 
        when "random"
          if @model.outputs.length is 1 then @model.outputs[0]
          else 
            @getrandom(@getchances())
            # farthest = -1
            # gns = @selfgraph.inodes
            # p = @model.pos
            # console.log @selfgraph
            # distances = @model.outputs.map((c) ->
            #                         d = distance(gns[c.id].model.pos, p)
            #                         if d > farthest then farthest = d
            #                         d
            # )
            # normalized = distances.map((dist) -> dist / farthest)

            # zerotoone = normalized.reduce(((a, v) -> a.concat([(if a.length is 0 then v else v + a[a.length-1])])), [])

            # console.log distances
            # console.log normalized
            # console.log zerotoone

  applynote : (note, ctrlnote) ->
    n = 
      if @model.type is "grapher"
        # console.log note
        # console.log ctrlnote
        ni = @igraph.applynote( ctrlnote)
        ni.note
      else
        (if @model.self.offset is "@" then ctrlnote else note) + @model.self.step
    r = {note : midiwrap(n), next: @exit()}
    r


    # {note : @model, next : if @model.outputs.length > 0 then @model.outputs[@counter] else "no_output" }
class igraph
  constructor : (graphs, g, @rootinggraph = null, @startnode = 0, @inodes = [], @currents = [0,0], @currentnote = 0) ->
    # console.log g
    # console.log @rootinggraph
    ns = if g is "rooting" then @rootinggraph else graphs[g].nodes
    # console.log ns
    # console.log graphs
    for n in ns
      @inodes.push(new inode($.extend(true, {}, n), ns, 0))
      if n.type is "grapher"
        @inodes[@inodes.length - 1].igraph = new igraph(graphs, n.self.graphname)
    # console.log @inodes
  applynote : (ctrlnote) ->
    # currentnote = note
    # console.log "in #{ctrlnote} current #{@currentnote}"
    applied = @inodes[@currents[1]].applynote(@currentnote, ctrlnote)
    @currentnote = applied.note
    if applied.next.type isnt "untied"
      @currents.shift()
      @currents.push applied.next.id
    else
      last = @currents[0]
      @currents.shift()
      @currents.push last

    @currentnote = midiwrap(applied.note)
    applied
  removecables : (type, id) ->
    for n in @inodes
      n.model.outputs = n.model.outputs.filter((e) -> (e.type isnt type or e.id isnt id))
      n.model.outputs = n.model.outputs.map((e) -> 
        if e.type is type and e.id > id then e.id -= 1
        e
      )
  remove : (n) ->
    if @inodes.length > 1
      if @startnode is n.id then @startnode = clamp(0, @inodes.length - 1, n.id - 1)
      if @startnode > n.id then @startnode.id -= 1
      @currents = [@startnode,@startnode]
      @removecables(n.type, n.id)
      if @rootinggraph?
        console.log "wtf"
      @inodes.splice(n.id, 1)
      @startnode
  reset : () ->
    @currents = [@startnode, @startnode]


class imidinode
  constructor : (@model, @midi = undefined) ->
    @midi = 
      if @model.self.type is "out"
        WebMidi.getOutputByName @model.self.name
      else 
        WebMidi.getInputByName @model.self.name
  move : (p) ->
    @model.pos = add2d(p, new v2(-128, -16))
  connect : (selected, pin) ->
    @model.outputs[pin] = 
      if @model.outputs[pin]? 
        null
      else
        __ selected
        t = if selected.object.type is "pinout" then "out" else "grapher"
        new cable(selected.id, t, pin, selected.object.pin)
  removecables : (n) ->
    @model.outputs = @model.outputs.map((o) -> 
      if o?
        if o.type isnt n.type or o.id isnt n.id
          new cable(o.id - (if o.id > n.id then 1 else 0), o.type, o.pin)
        else null
      else
        null
    )

class irooting
  constructor : (@proj, @igraph = {}, @inputs = [], @outputs = []) ->
    for i in @proj.rooting.inputs
      it = new imidinode($.extend(true, {}, i))
      @inputs.push it
    for o in @proj.rooting.outputs
      ot = new imidinode($.extend(true, {}, o))
      @outputs.push ot
    @igraph = new igraph(@proj.graphs, "rooting", @proj.rooting.graph.nodes)
  toggle: (prop, name) ->
    nt = @[prop].findIndex((e) -> e.model.self.name is name)
    if nt isnt -1
      if prop is "outputs"
        @igraph.removecables((if prop is "outputs" then "out" else "in"), nt)
        for i in @inputs
          i.removecables({})
      @[prop].splice(nt, 1)
    else
      t = if prop[0] is "i" then "in" else "out"
      mn =  new midinode(name, t)
      n = new node(t, mn, "",new v2(random() * 200,(if t is "out" then 1 else -1) * 200 + random() * 100), new Array(16).fill(null))
      @[prop].push(new imidinode(n))
  remove : (n) ->
    @igraph.remove(n)
    for i in @inputs
      i.removecables(n)
    for o in @outputs
      o.removecables(n)


copy = (o) -> $.extend(true, {}, o)
class iproject
  # graphs : {}
  constructor : (@model, @rooting = null) ->
    @rooting = new irooting(@model)
    # for k in Object.keys(@model.graphs)
      # @graphs[k] = new igraph(@model.graphs, k)
    __ @
  export : () ->
    graphs = {}
    @rooting.igraph.inodes.map((g) -> 
      graphs[g.model.self.graphname] = $.extend(true, {}, new graph(g.igraph.inodes.map((n)-> copy(n.model)))))
    new project(
                new rooting(
                            @rooting.inputs.map((i) -> copy(i.model)), 
                            @rooting.outputs.map((i) -> copy(i.model)),
                            @rooting.igraph.inodes.map((n) -> copy(n.model))),
                graphs
    )

# TODO
# remove notes
# random exiting







