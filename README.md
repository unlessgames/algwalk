# algwalk

A flow-graph based arpeggiator for creating algorithmic note patterns. 
It works by transforming incoming MIDI notes from inputs to outputs, to control software synths etc.

https://unlessgames.gitlab.io/algwalk

##### Setup

You need at least one MIDI input and output to make use of [algwalk](https://unlessgames.gitlab.io/algwalk), they can be either physical MIDI devices connected to your computer or virtual MIDI ports.

##### Creating virtual MIDI ports

| On **Windows** you can use [loopMIDI](https://www.tobias-erichsen.de/software/loopmidi.html)

| On **OSX** its possible to create them without any third-party software, as explained [here](https://hearandknow.wordpress.com/2010/05/09/iac-get-virtual-midi-ports-on-a-mac/).  

| On **Linux** do something like:
`$ modprobe snd-virmidi snd_index=1`
[(more info)](http://tldp.org/HOWTO/MIDI-HOWTO-10.html)

##### Rooting

After you have available MIDI ports they should show up in the PORTS menu in ROOTING. 
Enable/disable ports from there, connect an input's pin to a graph node and that node to a pin on an output.
Inputs have their pins (MIDI channels from 1-16) at the bottom while outputs at the top.

##### Editing
- **Create** new nodes by **right-clicking** on empty space
    ( if you are in ROOTING, this results in a creation of a new graph which you can then access from the left-side menu )

- **Move** nodes by **dragging** them

- **Connect** a node to another by dragging with right-click, starting from some node (you will see a line going from this start node to your cursor) and release mouse button on another node. This will create a one directional link from the start to the target, or if such connection was already existing then it will be removed.

- **Node properties:**
    Besides creating connections, you can set different properties of nodes. The keys for these are being shown around a highlighted node.
( they are centered around the _S_ key on the keyboard )
 
    - **W / S** - raise / lower the node's interval size. 
    - **A** - set node as **starting node** of the graph, a square shows which node is considered first.
    - **Q / @** - toggle the node's interval being **relative to input** note (for example the note that's coming in from a MIDI input port), or just adding to whatever note the previous node outputted.
    - **R** - toggle the **exiting** mechanism of the node being **random** or **sequential** (defined by the order in which outputs were connected) 
    This only matters on nodes with more than one connected output nodes.
    - **X** - **remove node** (in ROOTING this removes the whole graph)

Good Luck!

using [WebMidi](https://github.com/cotejp/webmidi) library for MIDI communication
